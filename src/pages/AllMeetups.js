import MeetupList from '../components/meetups/MeetupList';

const DUMMY_DATA = [
  {
    id: 'm1',
    title: 'This is a first meetup',
    image:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d3/Stadtbild_M%C3%BCnchen.jpg/2560px-Stadtbild_M%C3%BCnchen.jpg',
    address: 'Old Town of Munich',
    description:
      'This is a first, amazing meetup in Munich which you definitely should not miss. It will be a lot of fun!',
  },
  {
    id: 'm2',
    title: 'This is a second meetup',
    image:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e6/Shibuya_Crossing_%28181547621%29.jpeg/1920px-Shibuya_Crossing_%28181547621%29.jpeg',
    address: 'Shibuya Crossing, Tokyo',
    description:
      'This is a second, amazing meetup in Tokyo which you definitely should not miss. It will be a lot of fun!',
  },
  {
    id: 'm3',
    title: 'This is a third meetup',
    image:
      'https://upload.wikimedia.org/wikipedia/commons/4/4b/La_Tour_Eiffel_vue_de_la_Tour_Saint-Jacques%2C_Paris_ao%C3%BBt_2014_%282%29.jpg',
    address: 'Eiffel Tower, Paris',
    description:
      'This is a third, amazing meetup in Paris which you definitely should not miss. It will be a lot of fun!',
  },
];

function AllMeetupsPage() {
  return (
    <section>
      <h1>My Personal Meetups</h1>
      <MeetupList meetups={DUMMY_DATA} />
    </section>
  );
}

export default AllMeetupsPage;
